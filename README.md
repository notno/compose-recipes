```bash
git submodule add git@gitlab.com:notno/compose-recipes.git

git submodule update --recursive --remote

# elasticsearch + kibana
docker compose -f compose-recipes/elastic/compose.yml up -d

# kafka
docker compose -f compose-recipes/kafka/compose.yml up -d
# kafka + data persistence
docker compose -f compose-recipes/kafka/compose.yml -f compose-recipes/kafka/persistence.yml up -d
```
